plugins {
    GradlePluginId.apply {
        id(ANDROID_APPLICATION)
        kotlin(ANDROID)
        kotlin(KAPT)
        id(KOTLIN_HILT)
        id(NAVIGATION_SAFEARGS_KOTLIN)
        id(ANDROID_EXTENSIONS)
    }
}
kapt {
    correctErrorTypes = true
}
android {

    AppConfig.apply {

        compileSdkVersion(compileSdk)
        buildToolsVersion(buildToolsVersion)

        defaultConfig {


            applicationId = AppConfig.applicationId
            minSdkVersion(minimumSdkVersion)
            targetSdkVersion(targettSdkVersion)
            versionCode = versionCode
            versionName = versionName
            buildConfigField(
                "String",
                "BASE_URL",
                "\"https://api1.kiliaro.com\""
            )
            testInstrumentationRunner = androidTestInstrumentation
        }

        buildTypes {
            getByName(AppConfig.release) {
                isMinifyEnabled = false
                proguardFiles(
                    getDefaultProguardFile(proguardAndroidOptimize),
                    proguard
                )
            }
        }
    }
    compileOptions.sourceCompatibility = JavaVersion.VERSION_1_8
    compileOptions.targetCompatibility = JavaVersion.VERSION_1_8
    kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs =
            freeCompilerArgs.toMutableList().apply { add("-Xopt-in=kotlin.RequiresOptIn") }
    }
    kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
    buildFeatures.viewBinding = true
}

dependencies {

    //default
    implementation (Kotlin.kotlin_stdlib)
    implementation (AndroidX.extensionsCore)
    implementation (AndroidX.appCompat)
    implementation (AndroidX.material)
    implementation (AndroidX.constraintLayout)

    //coroutine
    implementation (Kotlin.Coroutine.core)
    implementation (Kotlin.Coroutine.coroutine)
    testImplementation (Kotlin.Coroutine.test)

    //fragment
    implementation (AndroidX.Fragment.fragment)
    implementation (AndroidX.Fragment.fragmentKtx)

    //lifecycle
    implementation (AndroidX.LifeCycle.liveData)
    implementation (AndroidX.LifeCycle.process)
    implementation (AndroidX.LifeCycle.runtime)
    implementation (AndroidX.LifeCycle.commonJava8)
    implementation (AndroidX.LifeCycle.viewModel)

    //core
    implementation (AndroidX.Arch.core)
    implementation (AndroidX.Arch.runtime)

    //room
    kapt (AndroidX.Room.compiler)
    implementation (AndroidX.Room.core)
    implementation (AndroidX.Room.runtime)

    //navigation
    implementation (AndroidX.Navigation.fragment)
    implementation (AndroidX.Navigation.uiKtx)
    implementation (AndroidX.Navigation.ui)
    implementation (AndroidX.Navigation.core)

    //network
    implementation (Network.OkHttp.core)
    implementation (Network.OkHttp.logger)
    testImplementation (Network.OkHttp.test)
    implementation (Network.Retrofit.core)
    implementation (Network.Retrofit.gsonConverter)

    //DI
    implementation(DependencyInjection.hiltAndroid)
    kapt(DependencyInjection.hiltAndroidCompiler)

    //glide
    implementation(AndroidX.Glide.glide)
    kapt(AndroidX.Glide.compiler)

    testImplementation (Test.junit)
    testImplementation (Test.core)
    androidTestImplementation (Test.junitKtx)
}