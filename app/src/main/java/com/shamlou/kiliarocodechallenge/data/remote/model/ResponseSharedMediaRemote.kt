package com.shamlou.kiliarocodechallenge.data.remote.model

import com.google.gson.annotations.SerializedName

data class ResponseSharedMediaRemote(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("user_id")
    var userId: String? = null,
    @SerializedName("media_type")
    var media_type: String? = null,
    @SerializedName("filename")
    var filename: String? = null,
    @SerializedName("size")
    var size: Long? = null,
    @SerializedName("created_at")
    var createdAt: String? = null,
    @SerializedName("taken_at")
    var takenAt: String? = null,//may be null
    @SerializedName("guessed_taken_at")
    var guessedTakenAt: String? = null,//may be null
    @SerializedName("md5sum")
    var md5sum: String? = null,
    @SerializedName("content_type")
    var contentType: String? = null,
    @SerializedName("video")
    var video: String? = null,//may be null
    @SerializedName("thumbnail_url")
    var thumbnailUrl: String? = null,
    @SerializedName("download_url")
    var downloadUrl: String? = null,
    @SerializedName("resx")
    var resx: Int? = null,
    @SerializedName("resy")
    var resy: Int? = null
)