package com.shamlou.kiliarocodechallenge.data.mapper

import com.shamlou.kiliarocodechallenge.data.local.entities.SharedMediaEntity
import com.shamlou.kiliarocodechallenge.data.remote.model.ResponseSharedMediaRemote
import com.shamlou.kiliarocodechallenge.util.Mapper
import javax.inject.Inject


class SharedMedialRemoteToEntity @Inject constructor() :
    Mapper<ResponseSharedMediaRemote, SharedMediaEntity> {
    override fun map(first: ResponseSharedMediaRemote): SharedMediaEntity {
        return first.run {
            SharedMediaEntity(
                id?:"",
                filename?:"",
                size?:0L,
                createdAt?:"",
                thumbnailUrl?:"",
                resx?:0,
                resy?:0
            )
        }
    }
}