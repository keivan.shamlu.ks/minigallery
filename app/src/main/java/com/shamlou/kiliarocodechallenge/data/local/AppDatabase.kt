package com.shamlou.kiliarocodechallenge.data.local

import androidx.room.*
import com.shamlou.kiliarocodechallenge.data.local.entities.SharedMediaEntity

@Database(entities = [SharedMediaEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun sharedMediaDao(): SharedMediaDao
}
