package com.shamlou.kiliarocodechallenge.data.remote.dataSource

import android.content.Context
import com.shamlou.kiliarocodechallenge.data.local.entities.SharedMediaEntity
import com.shamlou.kiliarocodechallenge.data.remote.api.MediaSetService
import com.shamlou.kiliarocodechallenge.data.remote.api.MediaSetService.Companion.apiKey
import com.shamlou.kiliarocodechallenge.data.remote.model.ResponseSharedMediaRemote
import com.shamlou.kiliarocodechallenge.util.Mapper
import com.shamlou.kiliarocodechallenge.data.util.Readable
import com.shamlou.kiliarocodechallenge.data.util.isNetworkAvailable
import dagger.hilt.android.qualifiers.ApplicationContext
import java.lang.Exception
import javax.inject.Inject

//a data source for decoupling remote from local , with a readble and a writable implementation
class RemoteSharedMediaDataSource @Inject constructor(
    private val mediaSetService: MediaSetService,
    private val sharedMediaRemoteToEntity: Mapper<ResponseSharedMediaRemote, SharedMediaEntity>,
    @ApplicationContext private val appContext : Context
) : RemoteSharedMediaDataSourceReadable {
    override suspend fun read(input: RemoteSharedMediaDataSourceReadable.Params): List<SharedMediaEntity> {

        if(appContext.isNetworkAvailable().not()) throw Exception("network not available")
        val data = mediaSetService.getSharedMedia(apiKey)
        return data.map { sharedMediaRemoteToEntity.map(it) }
    }
}

interface RemoteSharedMediaDataSourceReadable :
    Readable.Suspendable.IO<RemoteSharedMediaDataSourceReadable.Params, List<SharedMediaEntity>> {
    data class Params(val param : Unit)
}
