package com.shamlou.kiliarocodechallenge.data.repo

import com.shamlou.kiliarocodechallenge.data.local.SharedMediaDao
import com.shamlou.kiliarocodechallenge.data.local.dataSource.LocalSharedMediaDataSourceReadable
import com.shamlou.kiliarocodechallenge.data.local.dataSource.LocalSharedMediaDataSourceWritable
import com.shamlou.kiliarocodechallenge.data.local.entities.SharedMediaEntity
import com.shamlou.kiliarocodechallenge.data.remote.dataSource.RemoteSharedMediaDataSourceReadable
import com.shamlou.kiliarocodechallenge.data.util.networkWithCache
import com.shamlou.kiliarocodechallenge.domain.model.ResponseSharedMediaDomain
import com.shamlou.kiliarocodechallenge.domain.repo.MediaSetRepository
import com.shamlou.kiliarocodechallenge.util.Mapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

//centerizing remote and local to creating a readable and maintainable offline first system
class MediaSetRepositoryImpl @Inject constructor(
    private val localSharedMediaDataSourceReadable: LocalSharedMediaDataSourceReadable,
    private val localSharedMediaDataSourceWritable: LocalSharedMediaDataSourceWritable,
    private val remoteSharedMediaDataSourceReadable: RemoteSharedMediaDataSourceReadable,
    private val sharedMediaEntityToDomain: Mapper<SharedMediaEntity, ResponseSharedMediaDomain>,
    private val sharedMediaDao: SharedMediaDao
) : MediaSetRepository {

    override fun getMediaSet(param: Unit): Flow<List<ResponseSharedMediaDomain>> {
        return flow {
            val request = networkWithCache(
                createCall = {
                    remoteSharedMediaDataSourceReadable.read(
                        RemoteSharedMediaDataSourceReadable.Params(Unit)
                    )
                },
                loadFromLocal = {
                    localSharedMediaDataSourceReadable.read(
                        LocalSharedMediaDataSourceReadable.Params(Unit)
                    )
                },
                shouldFetch = {
                    localSharedMediaDataSourceReadable.read(LocalSharedMediaDataSourceReadable.Params(Unit)).isNullOrEmpty()
                },
                saveCallResult = {
                    localSharedMediaDataSourceWritable.write(
                        LocalSharedMediaDataSourceWritable.Params(
                            it
                        )
                    )
                }
            )
            emit(request.map { sharedMediaEntityToDomain.map(it) })
        }
    }

    override fun removeAll(): Flow<Unit> {

        return flow {
            sharedMediaDao.deleteAll().also {
                emit(Unit)
            }
        }
    }

}