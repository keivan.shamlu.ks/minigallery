package com.shamlou.kiliarocodechallenge.data.mapper

import com.shamlou.kiliarocodechallenge.data.local.entities.SharedMediaEntity
import com.shamlou.kiliarocodechallenge.domain.model.ResponseSharedMediaDomain
import com.shamlou.kiliarocodechallenge.util.Mapper
import javax.inject.Inject

class SharedMediaEntityToDomainMapper @Inject constructor() : Mapper<SharedMediaEntity, ResponseSharedMediaDomain> {
    override fun map(first: SharedMediaEntity): ResponseSharedMediaDomain {
        return first.run {
            ResponseSharedMediaDomain(id, size, createdAt, thumbnailUrl, resx, resy)
        }
    }
}