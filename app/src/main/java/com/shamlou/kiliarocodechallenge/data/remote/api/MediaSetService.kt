package com.shamlou.kiliarocodechallenge.data.remote.api

import com.shamlou.kiliarocodechallenge.data.remote.model.ResponseSharedMediaRemote
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MediaSetService {


    @GET("/shared/{sharedkey}/media")
    suspend fun getSharedMedia(@Path("sharedkey")sharedKey : String = apiKey): List<ResponseSharedMediaRemote>

    companion object {
        const val apiKey = "djlCbGusTJamg_ca4axEVw"
    }
}