package com.shamlou.kiliarocodechallenge.data.local.dataSource

import com.shamlou.kiliarocodechallenge.data.local.SharedMediaDao
import com.shamlou.kiliarocodechallenge.data.local.entities.SharedMediaEntity
import com.shamlou.kiliarocodechallenge.data.util.Writable
import com.shamlou.kiliarocodechallenge.data.util.Readable
import javax.inject.Inject

//a data source for decoupling local from remote , with a readble and a writable implementation
class LocalSharedMediaDataSource @Inject constructor(private val sharedMediaDao: SharedMediaDao) :
    LocalSharedMediaDataSourceReadable, LocalSharedMediaDataSourceWritable {
    override suspend fun read(input: LocalSharedMediaDataSourceReadable.Params): List<SharedMediaEntity> {
        return sharedMediaDao.getAll()
    }

    override suspend fun write(input: LocalSharedMediaDataSourceWritable.Params) {
        sharedMediaDao.insertAll(input.sharedMediaEntity)
    }

}

interface LocalSharedMediaDataSourceReadable : Readable.Suspendable.IO<LocalSharedMediaDataSourceReadable.Params, List<SharedMediaEntity>> {
    data class Params(val param : Unit)
}

interface LocalSharedMediaDataSourceWritable : Writable.Suspendable.IO<LocalSharedMediaDataSourceWritable.Params, Unit> {
    data class Params(val sharedMediaEntity: List<SharedMediaEntity>)
}
