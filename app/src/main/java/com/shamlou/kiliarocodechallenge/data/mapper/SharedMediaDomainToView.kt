package com.shamlou.kiliarocodechallenge.data.mapper

import android.content.Context
import com.shamlou.kiliarocodechallenge.domain.model.EnumResizeMode
import com.shamlou.kiliarocodechallenge.domain.model.ResponseSharedMediaDomain
import com.shamlou.kiliarocodechallenge.presentation.model.ResponseSharedMediaView
import com.shamlou.kiliarocodechallenge.util.Mapper
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject


class SharedMediaDomainToView @Inject constructor(
    @ApplicationContext private val context: Context,
) : Mapper<ResponseSharedMediaDomain , ResponseSharedMediaView> {
    override fun map(first: ResponseSharedMediaDomain): ResponseSharedMediaView {

        return first.run {
            ResponseSharedMediaView(id, size, createdAt, calculateOptimizedThumbnail(thumbnailUrl , resx , resy), getOptimizedWidth(), getOptimizedHeight(resx , resy)) }
    }

    private fun calculateOptimizedThumbnail(thumbnailUrl : String , resX : Int , resY : Int) : String{

        val builder = StringBuilder()
        builder.append(thumbnailUrl.run { substring(0, lastIndexOf("/")+1) })
        val width = getOptimizedWidth()
        val height = getOptimizedHeight(resX, resY)
        builder.append("?h=$height&w=$width&m=${EnumResizeMode.BOUNDING_BOX.type}")
        return builder.toString()
    }

    private fun getOptimizedWidth(): Int = context.resources.displayMetrics.widthPixels/3
    private fun getOptimizedHeight(resX : Int, resY : Int): Int {
        val width = getOptimizedWidth()
        val ratio = resY.toFloat()/resX.toFloat()
        return width.times(ratio).toInt()
    }
}