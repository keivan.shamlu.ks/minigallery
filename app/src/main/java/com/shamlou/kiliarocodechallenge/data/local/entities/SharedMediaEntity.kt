package com.shamlou.kiliarocodechallenge.data.local.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shared_media_set")
class SharedMediaEntity(
    @PrimaryKey
    val id: String,
    val filename: String,
    val size: Long,
    val createdAt: String,
    val thumbnailUrl: String,
    val resx: Int,
    val resy: Int
)