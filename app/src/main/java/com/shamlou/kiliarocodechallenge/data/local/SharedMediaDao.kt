package com.shamlou.kiliarocodechallenge.data.local

import androidx.room.Dao
import androidx.room.Query
import com.shamlou.kiliarocodechallenge.data.local.entities.SharedMediaEntity

@Dao
interface SharedMediaDao : BaseDao<SharedMediaEntity> {
    @Query("SELECT * FROM shared_media_set ")
    fun getAll(): List<SharedMediaEntity>

    @Query("SELECT * FROM shared_media_set order by id asc")
    fun getAllById(): List<SharedMediaEntity>

    @Query("SELECT * FROM shared_media_set order by createdAt asc")
    fun getAllByDate(): List<SharedMediaEntity>

    @Query("SELECT * FROM shared_media_set order by filename asc")
    fun getAllByName(): List<SharedMediaEntity>

    @Query("DELETE FROM shared_media_set ")
    fun deleteAll()

}