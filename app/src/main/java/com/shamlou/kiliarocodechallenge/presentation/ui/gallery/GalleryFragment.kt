package com.shamlou.kiliarocodechallenge.presentation.ui.gallery

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.shamlou.kiliarocodechallenge.R
import com.shamlou.kiliarocodechallenge.databinding.FragmentGalleryBinding
import com.shamlou.kiliarocodechallenge.domain.util.onError
import com.shamlou.kiliarocodechallenge.domain.util.onLoading
import com.shamlou.kiliarocodechallenge.domain.util.onSuccess
import com.shamlou.kiliarocodechallenge.presentation.ui.gallery.adapter.EqualGapItemDecoration
import com.shamlou.kiliarocodechallenge.presentation.ui.gallery.adapter.SharedMediaAdapter
import com.shamlou.kiliarocodechallenge.util.lifeCycle.autoCleared
import com.shamlou.kiliarocodechallenge.util.lifeCycle.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class GalleryFragment : Fragment(R.layout.fragment_gallery) {

    private val viewModel: GalleryViewModel by viewModels()
    private val binding by viewBinding(FragmentGalleryBinding::bind)
    private var mAdapter by autoCleared<SharedMediaAdapter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mAdapter = SharedMediaAdapter { clickedItem ->

            NavHostFragment.findNavController(this).navigate(GalleryFragmentDirections.fragmentGalleryToFragmentPhoto(clickedItem.thumbnailUrl , clickedItem.createdAt))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerviewSharedMedia.apply {
            adapter = mAdapter
            layoutManager = StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL).also {
                it.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
            }
            addItemDecoration(EqualGapItemDecoration(3 , 4))
        }

        binding.buttonRetry.setOnClickListener { viewModel.getSharedMedia() }
        binding.buttonRemove.setOnClickListener { viewModel.removeAll() }

        lifecycleScope.launchWhenCreated {
            viewModel.sharedMedia.collect {

                it.onLoading { isLoading ->
                    binding.progressbarLoading.visibility = if(isLoading) View.VISIBLE else View.GONE
                    if(isLoading) {
                        emptyStateVisible(false)
                        binding.buttonRemove.visibility = View.GONE
                    }
                }
                it.onSuccess { items->
                    mAdapter.submitList(items)
                    emptyStateVisible(false)
                    binding.buttonRemove.visibility = View.VISIBLE
                }
                it.onError {
                    mAdapter.submitList(emptyList())
                    Snackbar.make(binding.root, it.cause?.message?:it.message?:"" , Snackbar.LENGTH_LONG).show()
                    emptyStateVisible(true)
                    binding.buttonRemove.visibility = View.GONE
                }
            }
        }

    }
    private fun emptyStateVisible(isVisible : Boolean){
        binding.emptyState.visibility = if(isVisible)View.VISIBLE else View.GONE
        binding.buttonRetry.visibility = if(isVisible)View.VISIBLE else View.GONE
    }

}