package com.shamlou.kiliarocodechallenge.presentation.model

data class ResponseSharedMediaView(
    val id: String,
    val size: Long,
    val createdAt: String,
    val thumbnailUrl: String,
    val resx: Int,
    val resy: Int
)