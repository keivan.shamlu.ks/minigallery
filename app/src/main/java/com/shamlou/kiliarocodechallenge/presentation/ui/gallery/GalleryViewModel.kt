package com.shamlou.kiliarocodechallenge.presentation.ui.gallery

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.shamlou.kiliarocodechallenge.domain.model.EnumResizeMode
import com.shamlou.kiliarocodechallenge.domain.model.ResponseSharedMediaDomain
import com.shamlou.kiliarocodechallenge.domain.useCase.GetSharedMediaUseCase
import com.shamlou.kiliarocodechallenge.domain.useCase.RemoveAllUseCase
import com.shamlou.kiliarocodechallenge.domain.util.Resource
import com.shamlou.kiliarocodechallenge.domain.util.mapToList
import com.shamlou.kiliarocodechallenge.presentation.model.ResponseSharedMediaView
import com.shamlou.kiliarocodechallenge.util.Mapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class GalleryViewModel @Inject constructor(
    private val getSharedMediaUseCase : GetSharedMediaUseCase,
    private val removeAllUseCase : RemoveAllUseCase,
    private val sharedMediaDomainToView : Mapper<ResponseSharedMediaDomain , ResponseSharedMediaView>
) : ViewModel() {

    private val _sharedMedia = MutableSharedFlow<Resource<List<ResponseSharedMediaView>>>()
    val sharedMedia: SharedFlow<Resource<List<ResponseSharedMediaView>>>
        get() = _sharedMedia

    private val _removeMedia = MutableSharedFlow<Resource<Unit>>()
    val removeMedia: SharedFlow<Resource<Unit>>
        get() = _removeMedia

    init {


        getSharedMedia()
    }

    fun getSharedMedia() {
            viewModelScope.launch {
                _sharedMedia.emit(Resource.Loading(true))
                getSharedMediaUseCase(GetSharedMediaUseCase.Params(Unit)).map {
                    it.mapToList(sharedMediaDomainToView)
                }.collect {

                    _sharedMedia.emit(it)
                }
            }
    }

    fun removeAll(){

        viewModelScope.launch {
            _removeMedia.emit(Resource.Loading(true))
            removeAllUseCase(RemoveAllUseCase.Params(Unit)).collect {

                _removeMedia.emit(it)
                _sharedMedia.emit(Resource.Error(Exception("cache removed , you can fetch data again")))
            }
        }
    }

}