package com.shamlou.kiliarocodechallenge.presentation.ui.gallery.adapter

import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shamlou.kiliarocodechallenge.R
import com.shamlou.kiliarocodechallenge.databinding.ItemSharedMediaBinding
import com.shamlou.kiliarocodechallenge.presentation.model.ResponseSharedMediaView
import com.shamlou.kiliarocodechallenge.presentation.util.drawIconShapeBase


class SharedMediaAdapter(private val clicked : (ResponseSharedMediaView) -> Unit) :
    ListAdapter<ResponseSharedMediaView, SharedMediaViewHolder>(PLACE_COMPARATOR) {
    override fun onBindViewHolder(holder: SharedMediaViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it , clicked) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SharedMediaViewHolder {
        return SharedMediaViewHolder(
            ItemSharedMediaBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    //for better performance
    companion object {
        private val PLACE_COMPARATOR =
            object : DiffUtil.ItemCallback<ResponseSharedMediaView>() {
                override fun areItemsTheSame(
                    oldItem: ResponseSharedMediaView,
                    newItem: ResponseSharedMediaView
                ): Boolean =
                    oldItem == newItem


                override fun areContentsTheSame(
                    oldItem: ResponseSharedMediaView,
                    newItem: ResponseSharedMediaView
                ): Boolean =
                    oldItem == newItem
            }
    }
}

class SharedMediaViewHolder(private val view: ItemSharedMediaBinding) :
    RecyclerView.ViewHolder(view.root) {
    fun bind(item: ResponseSharedMediaView , clicked : (ResponseSharedMediaView) -> Unit) {

        view.textViewSize.apply {

            text = item.size.toString()
            drawIconShapeBase(ContextCompat.getColor(view.root.context, R.color.light_blue_300) , 0 , 0,0)
        }

        view.root.setOnClickListener { clicked(item) }
        //so item has it's actual size from begining
        view.root.layoutParams = ViewGroup.LayoutParams(item.resx,item.resy)
        //i always use a layer of abstraction for imageloading since there are several image
        // loading libraries and each time i have to trade off between them acording
        // on project that i'm working on
        Glide.with(view.root.context)
            .load(item.thumbnailUrl)
            .placeholder(ColorDrawable(ContextCompat.getColor(view.root.context, R.color.light_blue_300)))
            .override(item.resx,item.resy)
            .into(view.imageViewThumbnail)
    }
}
