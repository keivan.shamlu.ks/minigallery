package com.shamlou.kiliarocodechallenge.presentation.ui.photo

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.shamlou.kiliarocodechallenge.R
import com.shamlou.kiliarocodechallenge.databinding.FragmentPhotoBinding
import com.shamlou.kiliarocodechallenge.util.lifeCycle.viewBinding
import dagger.hilt.android.AndroidEntryPoint
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.shamlou.kiliarocodechallenge.presentation.util.drawIconShapeBase

@AndroidEntryPoint
class PhotoFragment : Fragment(R.layout.fragment_photo){

    private val binding by viewBinding(FragmentPhotoBinding::bind)
    private val args : PhotoFragmentArgs by navArgs()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //i would normally use formated string for this but i have no time
        binding.textViewCreatedAt.apply {
            text = "created at : ${args.createdAt}"
            drawIconShapeBase(ContextCompat.getColor(requireContext(), R.color.light_blue_300) , 0 , 0,30)
        }

        Glide.with(requireContext())
            .load(args.thumbnail)
            .placeholder(ColorDrawable(ContextCompat.getColor(requireContext(), R.color.light_blue_300)))
            .into(binding.imageViewThumbnail)
    }
}