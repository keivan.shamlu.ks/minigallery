package com.shamlou.kiliarocodechallenge.presentation.util

import android.content.res.Resources
import android.graphics.drawable.GradientDrawable
import android.view.View

//this fun draws background so we don't need to have a lot of shapes in resources
fun View.drawIconShapeBase(backColor: Int, strokeColor: Int, strokeWidth: Int?, corner: Int?){

    val c = corner ?: 0
    val stroke = strokeWidth ?: 0
    val shape = GradientDrawable().apply {
        shape = GradientDrawable.RECTANGLE
        cornerRadius = c.px
        setColor(backColor)
        setStroke(stroke.px.toInt(), strokeColor)
    }
    background = shape
}
val Int.px: Float
    get() = this.toFloat().px
val Float.px: Float
    get() = (this * Resources.getSystem().displayMetrics.density)