package com.shamlou.kiliarocodechallenge.presentation.ui.photo

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PhotoViewModel @Inject constructor() : ViewModel(){
}