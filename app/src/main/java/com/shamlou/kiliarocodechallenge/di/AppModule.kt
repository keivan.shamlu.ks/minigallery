package com.shamlou.kiliarocodechallenge.di

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

//app level dependencies here
@Module
@InstallIn(SingletonComponent::class)
object AppModule {

}