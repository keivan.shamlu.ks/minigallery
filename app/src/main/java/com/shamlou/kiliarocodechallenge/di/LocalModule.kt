package com.shamlou.kiliarocodechallenge.di

import android.content.Context
import androidx.room.Room
import com.shamlou.kiliarocodechallenge.data.local.AppDatabase
import com.shamlou.kiliarocodechallenge.data.local.SharedMediaDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

private const val APP_DATA_BASE = "APP_DATA_BASE"

@Module
@InstallIn(SingletonComponent::class)
object LocalModule {

    @Provides
    fun provideAppDataBase(
        @ApplicationContext applicationContext: Context
    ): AppDatabase {
        return Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, APP_DATA_BASE
        ).build()
    }

    @Provides
    fun provideAppDataBaseSharedMediaDao(
        dataBase: AppDatabase
    ): SharedMediaDao {
        return dataBase.sharedMediaDao()
    }
}