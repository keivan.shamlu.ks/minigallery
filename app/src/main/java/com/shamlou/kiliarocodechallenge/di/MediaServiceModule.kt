package com.shamlou.kiliarocodechallenge.di

import com.shamlou.kiliarocodechallenge.data.local.dataSource.LocalSharedMediaDataSource
import com.shamlou.kiliarocodechallenge.data.local.dataSource.LocalSharedMediaDataSourceReadable
import com.shamlou.kiliarocodechallenge.data.local.dataSource.LocalSharedMediaDataSourceWritable
import com.shamlou.kiliarocodechallenge.data.local.entities.SharedMediaEntity
import com.shamlou.kiliarocodechallenge.data.mapper.SharedMediaDomainToView
import com.shamlou.kiliarocodechallenge.data.mapper.SharedMediaEntityToDomainMapper
import com.shamlou.kiliarocodechallenge.data.mapper.SharedMedialRemoteToEntity
import com.shamlou.kiliarocodechallenge.data.remote.api.MediaSetService
import com.shamlou.kiliarocodechallenge.data.remote.dataSource.RemoteSharedMediaDataSource
import com.shamlou.kiliarocodechallenge.data.remote.dataSource.RemoteSharedMediaDataSourceReadable
import com.shamlou.kiliarocodechallenge.data.remote.model.ResponseSharedMediaRemote
import com.shamlou.kiliarocodechallenge.data.repo.MediaSetRepositoryImpl
import com.shamlou.kiliarocodechallenge.domain.model.ResponseSharedMediaDomain
import com.shamlou.kiliarocodechallenge.domain.repo.MediaSetRepository
import com.shamlou.kiliarocodechallenge.presentation.model.ResponseSharedMediaView
import com.shamlou.kiliarocodechallenge.util.Mapper
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MediaServiceModule {

    @Singleton
    @Provides
    fun provideMediaService(retrofit : Retrofit): MediaSetService {
        return retrofit.create(MediaSetService::class.java)
    }
}

@Module
@InstallIn(ViewModelComponent::class)
abstract class MediaServiceModuleAbs {

    @Binds
    abstract fun provideLocalSharedMediaDataSourceReadable(dataSource: LocalSharedMediaDataSource): LocalSharedMediaDataSourceReadable

    @Binds
    abstract fun provideLocalSharedMediaDataSourceWritable(dataSource: LocalSharedMediaDataSource): LocalSharedMediaDataSourceWritable

    @Binds
    abstract fun provideRemoteSharedMediaDataSourceReadable(dataSource: RemoteSharedMediaDataSource): RemoteSharedMediaDataSourceReadable

    @Binds
    abstract fun provideMediaSetRepositoryImpl(repository: MediaSetRepositoryImpl): MediaSetRepository

    @Binds
    abstract fun provideSharedMediaEntityToDomainMapper(mapper: SharedMediaEntityToDomainMapper): Mapper<SharedMediaEntity, ResponseSharedMediaDomain>

    @Binds
    abstract fun provideSharedMediaDomainToView(mapper: SharedMediaDomainToView) : Mapper<ResponseSharedMediaDomain , ResponseSharedMediaView>

    @Binds
    abstract fun provideSharedMedialRemoteToEntity(mapper: SharedMedialRemoteToEntity) : Mapper<ResponseSharedMediaRemote, SharedMediaEntity>
}