package com.shamlou.kiliarocodechallenge.util

//simple abstraction of mapping
interface Mapper<First, Second> {

    fun map(first: First): Second
}