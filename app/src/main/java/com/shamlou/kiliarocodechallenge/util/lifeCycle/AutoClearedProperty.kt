package com.shamlou.kiliarocodechallenge.util.lifeCycle

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

//a delegation that makes sure a varibale refrence is removed after
// fragment or activity(anny lifeccycle owner) destroyed
class AutoClearedProperty<T : Any>(AppCompatActivity: Fragment) : ReadWriteProperty<Fragment, T> {
    private var _value: T? = null

    init {
        AppCompatActivity.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                _value = null
            }
        })
    }

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        return _value ?: throw IllegalStateException(
            "should never call auto-cleared-value get when it might not be available"
        )
    }

    override fun setValue(thisRef: Fragment, property: KProperty<*>, value: T) {
        _value = value
    }
}

fun <T : Any> Fragment.autoCleared() =
    AutoClearedProperty<T>(this)
