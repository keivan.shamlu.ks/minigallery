package com.shamlou.kiliarocodechallenge.domain.model


data class ResponseSharedMediaDomain(
    val id: String,
    val size: Long,
    val createdAt: String,
    val thumbnailUrl: String,
    val resx: Int,
    val resy: Int
)