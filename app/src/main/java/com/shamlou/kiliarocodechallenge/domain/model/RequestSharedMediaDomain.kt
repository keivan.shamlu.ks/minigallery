package com.shamlou.kiliarocodechallenge.domain.model

enum class EnumResizeMode(val type : String){
    BOUNDING_BOX("bb"),//bounding box
    CROPED("crop"),//croped
    MINIMUM_DIMENSIONS("md")//minimum dimensions
}