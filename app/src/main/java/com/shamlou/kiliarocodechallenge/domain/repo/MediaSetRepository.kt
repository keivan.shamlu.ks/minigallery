package com.shamlou.kiliarocodechallenge.domain.repo

import com.shamlou.kiliarocodechallenge.domain.model.ResponseSharedMediaDomain
import kotlinx.coroutines.flow.Flow

//abstraction of repo here (domain) since we want to invert dependency flow so domain module be independent
//implemented at Data layer
interface MediaSetRepository {

    fun getMediaSet(param : Unit): Flow<List<ResponseSharedMediaDomain>>
    fun removeAll(): Flow<Unit>
}