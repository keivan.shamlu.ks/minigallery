package com.shamlou.kiliarocodechallenge.domain.useCase

import com.shamlou.kiliarocodechallenge.di.IoDispatcher
import com.shamlou.kiliarocodechallenge.domain.model.ResponseSharedMediaDomain
import com.shamlou.kiliarocodechallenge.domain.repo.MediaSetRepository
import com.shamlou.kiliarocodechallenge.domain.util.FlowUseCase
import com.shamlou.kiliarocodechallenge.domain.util.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject


//keeping application logics here in usecases
class GetSharedMediaUseCase @Inject constructor(
    @IoDispatcher dispatcher: CoroutineDispatcher,
    private val repository: MediaSetRepository
) :
    FlowUseCase<GetSharedMediaUseCase.Params, List<ResponseSharedMediaDomain>>(dispatcher) {

    data class Params(
        val param: Unit
    )

    override fun execute(parameters: Params): Flow<Resource<List<ResponseSharedMediaDomain>>> {
        return repository.getMediaSet(
            parameters.param
        ).map {
            Resource.Success(it)
        }
    }
}