object DependencyInjection {

    const val version = "2.38.1"
    const val hiltAndroid = "com.google.dagger:hilt-android:${version}"
    const val hiltAndroidCompiler = "com.google.dagger:hilt-android-compiler:${version}"
}