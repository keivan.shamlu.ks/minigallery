object AppConfig {
    const val compileSdk = 31
    const val minimumSdkVersion = 21
    const val targettSdkVersion = 31
    const val versionCode = 1
    const val versionName = "1.0.0"
    const val buildToolsVersion = "30.0.3"

    const val androidTestInstrumentation = "androidx.test.runner.AndroidJUnitRunner"
    const val applicationId = "com.shamlou.kiliarocodechallenge"
    const val proguard = "proguard-rules.pro"
    const val proguardAndroidOptimize = "proguard-android-optimize.txt"
    const val release = "release"
}