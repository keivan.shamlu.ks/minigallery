object GradlePluginId {
    const val ANDROID = "android"
    const val ANDROID_EXTENSIONS = "kotlin-android-extensions"
    const val ANDROID_APPLICATION = "com.android.application"
    const val KOTLIN_KAPT = "kotlin-kapt"
    const val KAPT = "kapt"
    const val NAVIGATION_SAFEARGS_KOTLIN = "androidx.navigation.safeargs.kotlin"
    const val KOTLIN_HILT = "dagger.hilt.android.plugin"
}
