object GradlePluginVersion {

    const val ANDROID_GRADLE = Gradle.androidGradleVersion
    const val KOTLIN = Kotlin.version
    const val SAFE_ARGS = AndroidX.Navigation.version
    const val HILT = DependencyInjection.version
}