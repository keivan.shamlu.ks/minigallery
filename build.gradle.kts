buildscript {
    repositories {
        mavenCentral()
        google()
        jcenter()
        mavenLocal()
        maven ("https://maven.google.com")
        maven ("https://jitpack.io")
    }
    dependencies {
        classpath(GradlePlugins.ANDROID_GRADLE)
        classpath(GradlePlugins.KOTLIN_GRADLE)
        classpath(GradlePlugins.SAFE_ARGS)
        classpath(GradlePlugins.HILT)
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        mavenCentral()
        google()
        jcenter()
        mavenLocal()
        maven ("https://maven.google.com")
        maven ("https://jitpack.io")
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}